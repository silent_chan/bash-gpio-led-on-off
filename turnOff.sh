 #!/bin/bash
#   Exports pin to userspace
echo 2 > /sys/class/gpio/export
# Sets pin 18 as an output
echo "out" > /sys/class/gpio/gpio2/direction
# Sets pin 18 to low
echo 0 > /sys/class/gpio/gpio2/value
echo 2 > /sys/class/gpio/unexport