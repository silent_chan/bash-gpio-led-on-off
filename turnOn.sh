#!/bin/bash
#   Exports pin to userspace
echo 2 > /sys/class/gpio/export
# Sets pin 18 as an output
echo "out" > /sys/class/gpio/gpio2/direction
# Sets pin 18 to high
echo 1 > /sys/class/gpio/gpio2/value
#close off
echo 2 > /sys/class/gpio/unexport
